from django.contrib import admin
from .models import TodosList
from .models import TodoItem

admin.site.register(TodosList)
admin.site.register(TodoItem)

# Register your models here.
