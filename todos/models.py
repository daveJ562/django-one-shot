from django.db import models


class TodosList(models.Model):
    name = models.CharField(max_length=200)
    created_on = models.DateTimeField(auto_now_add=True, null=True)

    def __str__(self):
        return self.name


class TodoItem(models.Model):
    task = models.CharField(max_length=100)
    due_date = models.DateTimeField(
        null=True,
        blank=True,
    )

    def __str__(self):
        return self.task
